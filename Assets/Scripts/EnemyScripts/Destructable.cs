﻿using UnityEngine;

public class Destructable : MonoBehaviour
{
    public float hitPoints;

    private float hitPointsCurrent;

    private void Start()
    {
        hitPointsCurrent = hitPoints;
    }

    public void Hit(float damage)
    {
        hitPointsCurrent -= damage;   //уменьшаю текущее колличество жизней

        if (hitPointsCurrent <= 0)  //если текущее значение жизни меньше или равно нулю, то:
        {
            Destroy(gameObject);

            HUD.Instance.UpdateScore(50);  //добавляю 50 очков

            HUD.Instance.ShowWindow(HUD.Instance.levelCompletedWindow);  //открываю окно что уровень пройден
        }
    }
}
﻿using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    Rigidbody shipEnemy;

    public float minSpeed, maxSpeed;  //объявление переменной скорости корабля

    public GameObject EnemyExplosions;

    public GameObject lazerShot;  //объект означающий чем будет стрелять, префаб лазера

    public GameObject lazerEnemyR;  //объект означающий откуда будет стрелять, правая пушка основная

    public GameObject lazerEnemyL;  //левая основная пушка

    public float shootDelay;  //задержка между выстрелами

    private float nextShot; //переменная для определения времени между выстрелами

    void Start()
    {
        shipEnemy = GetComponent<Rigidbody>();

        shipEnemy.velocity = Vector3.back * Random.Range(minSpeed, maxSpeed);
    }

    private void Update()
    {
        transform.rotation = Quaternion.Euler(0, 180, 0);  //задаю чтобы вражеский корабыль вылетал повёрнутым вниз и летел на корабль игрока
        
        if (Time.time > nextShot)
        {
            Instantiate(lazerShot, lazerEnemyL.transform.position, Quaternion.identity);

            Instantiate(lazerShot, lazerEnemyR.transform.position, Quaternion.identity);

            nextShot = Time.time + shootDelay;  //переставляем время следующего выстрела
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GameBoundary" || other.tag == "EnemyPlayer"  || other.tag == "Asteroid" || other.tag == "Bonus")
        {
            return;
        }

        if (other.tag == "Shield")
        {
            GameObject.FindGameObjectWithTag("Shield").SetActive(false);

            Destroy(gameObject);

            Instantiate(EnemyExplosions, transform.position, Quaternion.identity);
        }

        if (other.tag == "Player")
        {
            Instantiate(EnemyExplosions, other.transform.position, transform.rotation);

            Destroy(other.gameObject);

            Destroy(gameObject);

            HUD.Instance.ShowWindow(HUD.Instance.levelLoseWindow);
        }
        else
        {
            HUD.Instance.UpdateScore(10);  //добавляю очки
        }
    }
}
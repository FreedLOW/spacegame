﻿using UnityEngine;

public class LazerEnemyScript : MonoBehaviour
{
    public float spead;

    public GameObject EnemyExplosions;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = Vector3.back * spead;

        Destroy(gameObject, 5f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Asteroid"  || other.tag == "EnemyPlayer")
        {
            return;
        }
        else if (other.tag == "Shield")
        {
            GameObject.FindGameObjectWithTag("Shield").SetActive(false);

            Destroy(gameObject);
        }
        else if (other.tag == "Player")
        {
            Instantiate(EnemyExplosions, other.transform.position, Quaternion.identity);

            Destroy(other.gameObject);

            Destroy(gameObject);

            HUD.Instance.ShowWindow(HUD.Instance.levelLoseWindow);
        }
    }
}
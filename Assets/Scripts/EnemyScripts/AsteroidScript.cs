﻿using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    public float rotationSpeed;

    public float minSpeed, maxSpeed;  //скорость движения астероида

    public GameObject asteroidExsplosion;

    public GameObject shipExsplosions;
    
    void Start()
    {
        //реализуем вращение астероида
        var asteroid = GetComponent<Rigidbody>();

        asteroid.angularVelocity = Random.insideUnitSphere * rotationSpeed;

        //реализация движения астероида со случайной скоростью
        asteroid.velocity = Vector3.back * Random.Range(minSpeed, maxSpeed);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "GameBoundary" || other.tag == "Asteroid")
        {
            return;
        }
        else if(other.tag == "EnemyPlayer" || other.tag == "Bonus")
        {
            return;
        }
        
        if (other.tag == "Shield")
        {
            GameObject.FindGameObjectWithTag("Shield").SetActive(false);

            Destroy(gameObject);

            Instantiate(asteroidExsplosion, transform.position, Quaternion.identity);
        }

        if (other.tag == "Player")
        {
            Destroy(other.gameObject);

            Destroy(gameObject);

            Instantiate(shipExsplosions, transform.position, Quaternion.identity);

            Instantiate(asteroidExsplosion, transform.position, Quaternion.identity);

            HUD.Instance.ShowWindow(HUD.Instance.levelLoseWindow);
        }
        else
        {
            HUD.Instance.UpdateScore(5);  //обновляю очки, т.е. псоле уничтожения астероида добавится 5 очков
        }
    }
}
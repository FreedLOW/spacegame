﻿using UnityEngine;

public class BombScript : MonoBehaviour
{
    public float spead;

    public GameObject playerExplosions;

    public GameObject bombExplosions;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = Vector3.back * spead;

        transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GameBoundary" || other.tag == "Asteroid" || other.tag == "EnemyPlayer")
        {
            return;
        }
        else if (other.tag == "Shield")
        {
            GameObject.FindGameObjectWithTag("Shield").SetActive(false);

            Instantiate(bombExplosions, transform.position, Quaternion.identity);

            Destroy(gameObject);
        }
        else if (other.tag == "Player")
        {
            Instantiate(playerExplosions, other.transform.position, Quaternion.identity);

            Destroy(other.gameObject);

            Destroy(gameObject);

            HUD.Instance.ShowWindow(HUD.Instance.levelLoseWindow);
        }
    }
}
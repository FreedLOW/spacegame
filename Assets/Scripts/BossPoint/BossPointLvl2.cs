﻿using UnityEngine;

public class BossPointLvl2 : MonoBehaviour
{
    public GameObject boss;

    public GameObject emiter;

    private void Update()
    {
        if (HUD.Instance.score >= 200 && boss != null)
        {
            emiter.SetActive(false);
            boss.SetActive(true);
        }
        else if (boss == null)
            return;
    }
}
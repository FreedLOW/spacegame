﻿using UnityEngine;

public class BossPointLvl3 : MonoBehaviour
{
    public GameObject boss;

    public GameObject emiter;

    private void Update()
    {
        if (HUD.Instance.score >= 260 && boss != null)
        {
            emiter.SetActive(false);
            boss.SetActive(true);
        }
        else if (boss == null)
            return;
    }
}
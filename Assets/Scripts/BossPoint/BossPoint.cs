﻿using UnityEngine;

public class BossPoint : MonoBehaviour
{
    public GameObject boss;

    public GameObject emiter;

    private void Update()
    {
        if (HUD.Instance.score >= 120 && boss != null)
        {
            emiter.SetActive(false);
            boss.SetActive(true);
        }
        else if (boss == null)
            return;
    }
}
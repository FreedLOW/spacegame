﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossScriptLvl3 : MonoBehaviour
{
    public GameObject[] gun;

    public GameObject gunBomb;

    private bool seeTarget;

    private NavMeshAgent _navmeshagant;

    public Transform target;

    public float shotDelay;  //задержка между выстрелами

    private float nextShot;  //переменная для определения времени между выстрелами

    public GameObject lazerShot;

    public GameObject bombShoot;

    public GameObject bossExplosions;

    private Rigidbody bossRigidbody;

    private bool checkShoot = true;

    private void Start()
    {
        _navmeshagant = GetComponent<NavMeshAgent>();

        bossRigidbody = GetComponent<Rigidbody>();

        if (checkShoot)
        {
            Invoke("FirstShoot", 5f);  //первый выстрел будет через 5 сек после появления

            checkShoot = false;
        }

        InvokeRepeating("BossShootBomb", 10f, 8f);
    }

    private void Update()
    {
        if (target != null)
        {
            _navmeshagant.SetDestination(target.position);  //если в переменной есть значение, то задаю боссу цель в виде позиции игрока
        }

        transform.rotation = Quaternion.Euler(0, 180, 0);  //задаю чтобы вражеский корабыль вылетал повёрнутым вниз и летел на корабль игрока

        CheckTarget();  //вызываю метод в которм проверяется видит ли босс игрока

        if(checkShoot == false && !IsInvoking("FirstShoot"))
            ShotBoss();
    }

    public void ShotBoss()
    {
        if (seeTarget == true && Time.time > nextShot)
        {
            for (int i = 0; i < gun.Length; i++)
                Instantiate(lazerShot, gun[i].transform.position, Quaternion.identity);

            nextShot = Time.time + shotDelay;  //переставляем время следующего выстрела
        }
    }

    void FirstShoot()
    {
        for(int i = 0; i < gun.Length; i++)
        {
            Instantiate(lazerShot, gun[i].transform.position, Quaternion.identity);
        }
    }

    void BossShootBomb()
    {
        Instantiate(bombShoot, gunBomb.transform.position, Quaternion.identity);
    }

    public void CheckTarget()
    {
        for (int i = 0; i < gun.Length; i++)
        {
            if (target != null)
            {
                Vector3 targetDirection = target.position - gun[i].transform.position;

                Ray ray = new Ray(gun[i].transform.position, targetDirection);

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform == target)
                    {
                        seeTarget = true;
                        return;
                    }
                }
                seeTarget = false;
            }
        }
    }
}
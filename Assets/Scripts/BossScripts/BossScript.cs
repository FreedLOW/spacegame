﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossScript : MonoBehaviour
{
    public GameObject[] gun;

    private bool seeTarget;

    private NavMeshAgent _navmeshagant;

    public Transform target;

    public float shotDelay;  //задержка между выстрелами

    private float nextShot;  //переменная для определения времени между выстрелами

    public GameObject lazerShot;

    public GameObject bossExplosions;

    private Rigidbody bossRigidbody;

    private bool checkShoot = true;

    private void Start()
    {
        _navmeshagant = GetComponent<NavMeshAgent>();

        bossRigidbody = GetComponent<Rigidbody>();

        if (checkShoot == true)
        {
            Invoke("FirstShoot", 5f);  //первый выстрел будет через 5 сек после появления

            checkShoot = false;
        }
    }

    private void Update()
    {
        if (target != null)
        {
            _navmeshagant.SetDestination(target.position);
        }

        transform.rotation = Quaternion.Euler(0, 180, 0);  //задаю чтобы вражеский корабыль вылетал повёрнутым вниз и летел на корабль игрока

        CheckTarget();

        if (checkShoot == false && !IsInvoking("FirstShoot"))
            ShotBoss();
    }

    public void ShotBoss()  //метод в котором реализован выстрел босса с определённой задержкой
    {
        if (seeTarget == true && Time.time > nextShot)
        {
            for (int i = 0; i < gun.Length; i++)
            {
                Instantiate(lazerShot, gun[i].transform.position, Quaternion.identity);
            }

            nextShot = Time.time + shotDelay;  //переставляем время следующего выстрела
        }
    }

    void FirstShoot()
    {
        for (int i = 0; i < gun.Length; i++)
        {
            Instantiate(lazerShot, gun[i].transform.position, Quaternion.identity);
        }
    }

    public void CheckTarget()
    {
        for(int i=0; i < gun.Length; i++)
        {
            if (target != null)
            {
                Vector3 targetDirection = target.position - gun[i].transform.position;

                Ray ray = new Ray(gun[i].transform.position, targetDirection);

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform == target)
                    {
                        seeTarget = true;
                        return;
                    }
                }
                seeTarget = false;
            }
        }
    }
}
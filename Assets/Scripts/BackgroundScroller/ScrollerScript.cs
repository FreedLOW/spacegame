﻿using UnityEngine;

public class ScrollerScript : MonoBehaviour
{
    private Vector3 startPosition;  //стартовая позиция фона

    public float speed;  //переменная для контроля скорости перемещения (скролинга)
    
    void Start()
    {
        startPosition = transform.position;
    }
    
    void Update()
    {
        //переменная для определения на сколько нужно дфигать фон
        float move = Mathf.Repeat(Time.time * speed, 150);  //матф.рэпит позволяет зациклить значение, чтобы оно не выходило за нужные границы, что и написано в строке, где 150 - макс. значение движения фона

        //меняем позицию фона
        transform.position = startPosition + Vector3.back * move;  //к начальной позиции добавляем что фон должен двигаться вниз и домнажаем на move который определяет премещение фона
    }
}
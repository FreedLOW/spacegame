﻿using UnityEngine;

public class LazerScript : MonoBehaviour
{
    private float damage;

    public GameObject enemyExplosionEffect;

    public GameObject asteroidExplosion;

    public float spead;

    public float Damage { get => damage; set => damage = value; }
    
    void Start()
    {
        GetComponent<Rigidbody>().velocity = Vector3.forward * spead;
    }

    private void OnTriggerEnter(Collider other)
    {
        Destructable target = other.gameObject.GetComponent<Destructable>();

        if (other.tag == "GameBoundary" || other.tag == "Player")
        {
            return;
        }
        else if (other.tag == "Asteroid")
        {
            Instantiate(asteroidExplosion, other.transform.position, Quaternion.identity);

            Destroy(other.gameObject);

            Destroy(gameObject);
        }
        else if (other.tag == "EnemyPlayer")
        {
            Instantiate(enemyExplosionEffect, other.transform.position, Quaternion.identity);

            Destroy(other.gameObject);

            Destroy(gameObject);
        }
        else if (target != null && other.tag == "Boss")
        {
            target.Hit(Damage);

            Destroy(gameObject);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpaceSript : MonoBehaviour
{
    public GameObject shield;

    private Rigidbody ship;

    public float speed;

    public float tilt; //коэфициент поворота

    public float xMin, xMax, zMin, zMax;  //переменный регулировки перемещения корабля, т.е в каком диапазоне он может двигаться на карте

    public GameObject lazerShotPrefab;  //объект означающий чем будет стрелять, префаб лазера
    public GameObject lazerGunR;  //объект означающий откуда будет стрелять, правая пушка основная
    public GameObject lazerGunL;  //левая основная пушка
    public GameObject miniLazerShotPrefab;
    public GameObject miniGunR;
    public GameObject miniGunL;

    public float shotDelay;  //задержка между выстрелами

    private float nextShot; //переменная для определения времени между выстрелами

    public float damageLazerL, damageLazerR;

    void Start()
    {
        ship = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        var moveHorizontal = Input.GetAxis("Horizontal");

        var moveVertical = Input.GetAxis("Vertical");

        ship.velocity = new Vector3(moveHorizontal, 0, moveVertical) * speed;  //скорость корабля

        //настройка наклона коробля при поворотах и движений вперёд и назад:
        ship.rotation = Quaternion.Euler(ship.velocity.z * tilt, 0, -ship.velocity.x * tilt);

        //ограничиваем зону полёта коробля, т.е. за какие пределы он не вылетит:
        var positionX = Mathf.Clamp(ship.position.x, xMin, xMax);
        var positionZ = Mathf.Clamp(ship.position.z, zMin, zMax);
        var positionY = ship.position.y;  //ось у никак не надо ограничивать

        //теперь ограниченую позицию нужно положить обратно к кораблю:
        ship.position = new Vector3(positionX, positionY, positionZ);

        if (HUD.Instance.gameMenu.alpha == 0 && HUD.Instance.settingsMenu.alpha == 0
            && HUD.Instance.levelCompletedWindow.alpha == 0 && HUD.Instance.levelLoseWindow.alpha == 0)
        {
            ToShoot();
        }        
    }

    //метод реализации выстрела из орудий:
    void ToShoot()
    {
        if (Time.time > nextShot && Input.GetButton("Fire1"))
        {
            GameObject lazerR = Instantiate(lazerShotPrefab, lazerGunR.transform.position, Quaternion.identity);  //выстрел, используя этот метод сразу идёт тот объект которым хотим стрелять, далее задаётся позиция откуда будет производится выстрел, и его углы наклон в данном случае они не нужны

            GameObject lazerL = Instantiate(lazerShotPrefab, lazerGunL.transform.position, Quaternion.identity);

            nextShot = Time.time + shotDelay;  //переставляем время следующего выстрела

            lazerL.GetComponent<LazerScript>().Damage = damageLazerL;

            lazerR.GetComponent<LazerScript>().Damage = damageLazerR;
        }

        if (Time.time > nextShot && Input.GetButton("Fire2"))
        {
            GameObject lazerR = Instantiate(miniLazerShotPrefab, miniGunR.transform.position, Quaternion.identity);

            GameObject lazerL = Instantiate(miniLazerShotPrefab, miniGunL.transform.position, Quaternion.identity);

            nextShot = Time.time + shotDelay;

            lazerL.GetComponent<LazerScript>().Damage = damageLazerL;

            lazerR.GetComponent<LazerScript>().Damage = damageLazerR;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Bonus")
            shield.SetActive(true);
    }
}